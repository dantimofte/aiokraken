import os
import asyncio
import signal
from aiokraken import RestClient
from aiokraken.utils import get_kraken_logger


KEY = os.environ.get('KRAKEN_KEY')
SECRET = os.environ.get('KRAKEN_SECRET')
LOGGER = get_kraken_logger(__name__)


async def asset_pairs() -> None:
    """Start kraken websockets api
    """
    print('starting main')
    rest_kraken = RestClient()
    data = {
        "info": "altname",
        "pair": "ADACAD"
    }
    response = await rest_kraken.public_request('AssetPairs', data=data)
    await rest_kraken.close()
    # my_result = len(response["result"])
    print(f'response is {response}')


async def balance() -> None:
    """Start kraken websockets api
    """
    rest_kraken = RestClient(KEY, SECRET)
    response = await rest_kraken.balance()
    await rest_kraken.close()
    print(f'response is {response}')


async def get_assets() -> None:
    """Start kraken websockets api
    """
    rest_kraken = RestClient(KEY, SECRET)
    response = await rest_kraken.asset_pairs()
    await rest_kraken.close()
    print(f'response is {response}')

@asyncio.coroutine
def ask_exit(sig_name):
    print("got signal %s: exit" % sig_name)
    yield from asyncio.sleep(1.0)
    asyncio.get_event_loop().stop()


loop = asyncio.get_event_loop()

loop.create_task(
    get_assets()
)
for signame in ('SIGINT', 'SIGTERM'):
    loop.add_signal_handler(
        getattr(signal, signame),
        lambda: asyncio.ensure_future(ask_exit(signame))
    )
loop.run_forever()
