""" AIOKraken rest client """
import asyncio
import urllib
import hashlib
import hmac
import base64
import aiohttp
from aiokraken.utils import get_kraken_logger, get_nonce

BASE_URL = 'https://api.kraken.com'
LOGGER = get_kraken_logger(__name__)


class RestClient:

    def __init__(self, key=None, secret=None):
        self.key = key
        self.secret = secret

        headers = {
            'User-Agent': 'aiokraken'
        }
        self.session = aiohttp.ClientSession(headers=headers, raise_for_status=True)

        private_endpoints = {
            'balance': 'Balance'
        }
        self.add_public_endpoints_methods()
        # add dynamic methods to RestClient
        # RestClient.balance() will translate to RestClient.private_request('Balance')
        for fn_name, endpoint in private_endpoints.items():

            async def fn(self):
                resp = await self.private_request(endpoint)
                return resp
            setattr(RestClient, fn_name, fn)
            fn.__name__ = fn_name

    def add_public_endpoints_methods(self):
        """ add dynamic methods to RestClient
             RestClient.balance() will translate to RestClient.private_request('Balance')
        """
        public_endpoints = {
            'asset_pairs': 'AssetPairs'
        }
        for fn_name, endpoint in public_endpoints.items():

            async def fn(self):
                resp = await self.public_request(endpoint)
                return resp
            setattr(RestClient, fn_name, fn)
            fn.__name__ = fn_name

    async def public_request(self, endpoint, data=None):
        """ make public requests to kraken api"""
        async with self.session.post(f'{BASE_URL}/0/public/{endpoint}', data=data) as response:
            if response.status not in (200, 201, 202):
                return {'error': response.status}
            else:
                res = await response.json(encoding='utf-8', content_type=None)
                return res

    async def private_request(self, endpoint, data={}):
        """ make public requests to kraken api"""
        data['nonce'] = get_nonce()

        url_path = f"/0/private/{endpoint}"
        headers = {
            'API-Key': self.key,
            'API-Sign': self._sign_message(data, url_path)
        }

        async with self.session.post(
                f'{BASE_URL}{url_path}',
                data=data,
                headers=headers) as response:
            if response.status not in (200, 201, 202):
                return {'error': response.status}
            else:
                res = await response.json(encoding='utf-8', content_type=None)
                return res

    def _sign_message(self, data, url_path):
        """
            Kraken message signature for private user endpoints
            https://www.kraken.com/features/api#general-usage
        """
        post_data = urllib.parse.urlencode(data)

        # Unicode-objects must be encoded before hashing
        encoded = (str(data['nonce']) + post_data).encode()
        message = url_path.encode() + hashlib.sha256(encoded).digest()
        signature = hmac.new(
            base64.b64decode(self.secret),
            message,
            hashlib.sha512
        )
        sig_digest = base64.b64encode(signature.digest())

        return sig_digest.decode()

    async def close(self):
        """ Close aiohttp session """
        await self.session.close()
